package interactors

import "productoffers/services/pq2mq/domain"

// Dispatcher ...
type Dispatcher struct {
	domain.IListener
}

// NewDispatcher ...
func NewDispatcher(listener domain.IListener, publisher domain.IPublisher) *Dispatcher {
	interactor := new(Dispatcher)

	interactor.IListener = listener

	listener.SetPublisher(publisher)

	return interactor
}
