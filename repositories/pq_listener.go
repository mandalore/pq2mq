package repositories

import (
	"database/sql"
	"encoding/json"
	"log"
	"productoffers/services/pq2mq/domain"
	"time"

	"github.com/lib/pq"
	uuid "github.com/satori/go.uuid"
)

// PqListener ...
type PqListener struct {
	pq        *pq.Listener
	db        *sql.DB
	pub       domain.IPublisher
	pqChannel string
	done      chan bool
}

// NewPqListener ...
func NewPqListener(conninfo string, pqChannel string) (*PqListener, error) {
	pqListener := new(PqListener)

	errorHandler := func(ev pq.ListenerEventType, err error) {
		if err != nil {
			log.Fatal(err.Error())
		}
	}

	db, err := sql.Open("postgres", conninfo)
	if err != nil {
		return nil, err
	}

	pqListener.done = make(chan bool)

	pqListener.db = db
	pqListener.pq = pq.NewListener(conninfo, 10*time.Second, time.Minute, errorHandler)
	pqListener.pqChannel = pqChannel

	return pqListener, nil
}

// SetPublisher ...
func (listener *PqListener) SetPublisher(pub domain.IPublisher) {
	listener.pub = pub
}

// Dispatch ...
func (listener *PqListener) Dispatch(workers int) error {
	err := listener.pq.Listen(listener.pqChannel)
	if err != nil {
		return err
	}

	wqueue := make(chan []byte, 32)

	go listener.pqHandleNotification(wqueue)

	for i := 0; i < workers; i++ {
		go listener.pqNotificationDispatch(wqueue, i)
	}

	// Wait
	<-listener.done

	return nil
}

func (listener *PqListener) pqHandleNotification(queue chan []byte) {
	log.Println("Starting Pq Listener")
	runs := 0

	for {
		select {
		case raw := <-listener.pq.Notify:
			if raw == nil {
				continue
			}

			queue <- []byte(raw.Extra)
		case <-time.After(60 * time.Second):
			if runs%2 == 0 {
				go func() {
					log.Println("Republishing")
					if err := listener.republish(); err != nil {
						log.Fatal(err)
					}
				}()
			}
			go func() {
				err := listener.pq.Ping()
				if err != nil {
					log.Fatal(err)
				}
			}()
		}
	}
}

func (listener *PqListener) republish() error {
	if _, err := listener.db.Exec(`
		UPDATE es_events 
		SET is_dispatched = 'f' 
		WHERE is_dispatched = 'f'
	`); err != nil {
		return err
	}

	return nil
}

func (listener *PqListener) pqNotificationDispatch(queue chan []byte, i int) error {
	runs := 0
	log.Println("Starting Publisher")
	for {
		select {
		case raw := <-queue:
			log.Println("Handled by worker", i)

			var evt domain.PqEvent
			if err := json.Unmarshal(raw, &evt); err != nil {
				log.Println(err)
				continue
			}

			err := listener.dispatchEvent(evt)
			if err != nil {
				log.Println("Failed to process event", err)
			}
		case <-time.After(10 * time.Second):
			go func() {
				if err := listener.pub.Ping(); err != nil {
					log.Fatal("No connection to NSQ")
				}
			}()
		}
		runs++
	}
}

func (listener *PqListener) dispatchEvent(evt domain.PqEvent) error {
	var eventID uuid.UUID

	tx, err := listener.db.Begin()
	if err != nil {
		return err
	}

	row := tx.QueryRow(`
		SELECT event_id
		FROM es_events
		WHERE event_id = $1 AND is_dispatched = 'f'
    	FOR UPDATE NOWAIT
    `, evt.EventID.String())

	if err := row.Scan(&eventID); err != nil {
		tx.Rollback()
		return err
	}

	if err := listener.pub.Publish(evt); err != nil {
		tx.Rollback()
		return err
	}

	if _, err := tx.Exec(`
		UPDATE es_events 
		SET is_dispatched = 't' 
		WHERE event_id = $1 AND is_dispatched = 'f'
	`, evt.EventID.String()); err != nil {
		return err
	}

	if err := tx.Commit(); err != nil {
		return err
	}

	return nil
}
