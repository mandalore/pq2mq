package domain

import (
	"encoding/json"

	uuid "github.com/satori/go.uuid"
)

// IListener ...
type IListener interface {
	Dispatch(int) error
	SetPublisher(IPublisher)
}

// IPublisher ...
type IPublisher interface {
	Ping() error
	Publish(PqEvent) error
}

// PqEvent ...
type PqEvent struct {
	Stream      string           `json:"aggregate_type"`
	AggregateID uuid.UUID        `json:"aggregate_id"`
	EventID     uuid.UUID        `json:"event_id"`
	Version     int              `json:"aggregate_version"`
	Type        string           `json:"event_type"`
	Payload     *json.RawMessage `json:"data"`
}
