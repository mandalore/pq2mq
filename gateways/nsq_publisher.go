package gateways

import (
	"encoding/json"
	"productoffers/services/pq2mq/domain"

	nsq "github.com/nsqio/go-nsq"
)

// NSQPublisher ...
type NSQPublisher struct {
	pub *nsq.Producer
}

// NewNSQPublisher ...
func NewNSQPublisher(pub *nsq.Producer) (*NSQPublisher, error) {
	publisher := new(NSQPublisher)

	publisher.pub = pub

	return publisher, nil
}

// Publish ...
func (publisher *NSQPublisher) Publish(evt domain.PqEvent) error {
	raw, err := json.Marshal(evt)
	if err != nil {
		return err
	}

	return publisher.pub.Publish(evt.Stream, raw)
}

// Ping ...
func (publisher *NSQPublisher) Ping() error {
	return publisher.pub.Ping()
}
