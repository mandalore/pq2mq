package gateways

import (
	"log"
	"productoffers/services/pq2mq/domain"
)

// LogPublisher ...
type LogPublisher struct{}

// NewLogPublisher ...
func NewLogPublisher() (*LogPublisher, error) {
	return &LogPublisher{}, nil
}

// Publish ...
func (publisher *LogPublisher) Publish(evt domain.PqEvent) error {
	log.Println(evt)

	return nil
}
